export function importImg (req) {
  const files = {}
  req.keys().forEach(key => {
    if (key.indexOf('@2x') !== -1) return

    files[key.replace(/^\.\/img[-_]|(\.png|\.svg)$/g, '')] = req(key)
  })
  return files
}
